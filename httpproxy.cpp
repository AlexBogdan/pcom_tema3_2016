#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <netdb.h>
#include <iostream>
#include <errno.h>
#include <sstream>

#define MAX_CLIENTS 5
#define MAXLEN 1500

using namespace std;

void error(char *msg)
{
    perror(msg);
    exit(1);
}

/**
 * Citeste maxim maxlen octeti din socket-ul sockfd. Intoarce
 * numarul de octeti cititi.
 */
ssize_t Readline(int sockd, void * vptr, size_t maxlen) {
    ssize_t n, rc;
    char c, * buffer;

    buffer = (char*) vptr;
    memset(vptr, 0, MAXLEN);

    for (n = 1; n < maxlen; n++) {
        if ((rc = read(sockd, & c, 1)) == 1) { * buffer++ = c;
            if (c == '\n')
                break;
        } else if (rc == 0) {
            if (n == 1)
                return 0;
            else
                break;
        } else {
            if (errno == EINTR)
                continue;
            return -1;
        }
    }

    * buffer = 0;
    return n;
}

/**
 * Trimite o comanda HTTP si asteapta raspuns de la server.
 * Comanda trebuie sa fie in buffer-ul sendbuf.
 * Sirul expected contine inceputul raspunsului pe care
 * trebuie sa-l trimita serverul in caz de succes (de ex. codul
 * 200). Daca raspunsul semnaleaza o eroare se iese din program.
 */
void send_command(int sockfd, char sendbuf[], char * expected) {
    int nbytes;
    char CRLF[3];
    CRLF[0] = 13; CRLF[1] = 10; CRLF[2] = 0;
    strcat(sendbuf, CRLF);
    write(sockfd, sendbuf, strlen(sendbuf));
}

char recvbuf[MAXLEN];

void recv_msg(int serverfd, int clientfd, char * expected) {
    int nbytes;
    do {
        nbytes = Readline(serverfd, recvbuf, MAXLEN - 1);
        if (nbytes <= 0) break;
        recvbuf[nbytes] = 0;
        send(clientfd, recvbuf, strlen(recvbuf), 0);
    } while(1);
}

bool is_end(string& line) {
	string comp("Connection: close");
	return line.rfind(comp) == 0;
}

int main(int argc, char *argv[])
{
    int sockfd, newsockfd, serversockfd, port;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    int n, i, j;

    char server_ip[10];
    char buffer[MAXLEN];
    char sendbuf[MAXLEN];
    char recvbuf[MAXLEN];

    if (argc != 2) {
        printf("./httpproxy <port_proxy>");
        exit(-1);
    }

    port = atoi(argv[1]);

    fd_set read_fds;	//multimea de citire folosita in select()
    fd_set tmp_fds;	//multime folosita temporar 
    int fdmax;		//valoare maxima file descriptor din multimea read_fds

    // golim multimea de descriptori de citire (read_fds) si multimea tmp_fds 
    FD_ZERO(&read_fds);
    FD_ZERO(&tmp_fds);
    
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
       error((char*) "ERROR opening socket");

    memset((char *) &serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;	// foloseste adresa IP a masinii
    serv_addr.sin_port = htons(port);
    
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr)) < 0) 
            error((char*) "ERROR on binding");
     
    listen(sockfd, MAX_CLIENTS);

    //adaugam noul file descriptor (socketul pe care se asculta conexiuni) in multimea read_fds
    FD_SET(0, &read_fds);
    FD_SET(sockfd, &read_fds);
    fdmax = sockfd;

    // main loop
    char cmdParts[6][16];
    char *token;
    int card;
    int index_user;
    float sold;
    char welcome[35];
    char filename[32];
    FILE *log;  // Fisierul in care vom scrie logul
    int last_sent_card;

	while (1) {
		tmp_fds = read_fds; 
		if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) 
			error((char*) "ERROR in select");
		
		if (FD_ISSET(0, &tmp_fds)) { // Citirea de la tastatura
			memset(buffer, 0 , MAXLEN);  // Primim o comanda noua
            fgets(buffer, MAXLEN-1, stdin);
		}
	
		for(i = 1; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_fds)) {
				if (i == sockfd) {
					// a venit ceva pe socketul inactiv(cel cu listen) = o noua conexiune
					// actiunea serverului: accept()
					clilen = sizeof(cli_addr);
					if ((newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen)) == -1) {
						error((char*) "ERROR in accept");
					} 
					else {
						//adaug noul socket intors de accept() la multimea descriptorilor de citire
						FD_SET(newsockfd, &read_fds);
						if (newsockfd > fdmax) { 
							fdmax = newsockfd;
						}
					}
					printf("Noua conexiune de la %s, port %d, socket_client %d\n ", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port), newsockfd);
				}
			
				else {
					// am primit date pe unul din socketii cu care vorbesc cu clientii
					//actiunea serverului: recv()
					memset(filename, 0, 32);  // Formam numele fisierului de log
					strcpy(filename, "client-");
					//snprintf(filename +7, "%d", i);
					strcpy(filename + strlen(filename), ".log");
					log = fopen(filename, "a");  // Descriem fisierul de log pentru scriere

					memset(buffer, 0, MAXLEN);
					if ((n = Readline(i, recvbuf, MAXLEN -1)) <= 0) {
						if (n == 0) {
							//conexiunea s-a inchis
							printf((char*) "selectserver: socket %d hung up\n", i);
						} else {
							error((char*) "ERROR in recv");
						}

						close(i); 
						FD_CLR(i, &read_fds); // scoatem din multimea de citire socketul pe care 
					} else { // Rezolvare tema 3 -- parsam si trimitem comanda
						// ------------------ Facem citirea si parsarea comenzii -----------------
				 		string line(recvbuf);
				 		if (line.length() == 2) {
				 			close(serversockfd);
				 			close(i);
							FD_CLR(i, &read_fds); // Inchidem canalul cu procesul
						    continue;
				 		}
				 		if (is_end(line)) {
				 			sprintf(sendbuf, "Connection: close\n\n");
						    send_command(serversockfd, sendbuf, (char*) "200");
					    	recv_msg(serversockfd, i, (char*) "200");
						    continue;
				 		}

				 		string cmd_parts[3];
				 		int size = 0;
					    stringstream in(line);
					    while (in.good() && size < 3){
					        in >> cmd_parts[size];
					        ++size;
					    }

					    char method[cmd_parts[0].length()];
				    	strcpy(method, cmd_parts[0].c_str());
				    	char http[8 +1];
				    	strcpy(http, "HTTP/1.0");

					    for(int j = 0; j < size; j++){
					        cout << cmd_parts[j] << endl;
					    }
			// ------------------------ Interpretam comanda -------------------------

					    string url_head = cmd_parts[1].substr(0, 7);
					    cout << "URL_HEAD : " << url_head << endl;
				    	if (url_head.compare("http://") == 0) {
							cout << cmd_parts[1] << endl;
							cmd_parts[1] = cmd_parts[1].substr(7, cmd_parts[1].length());
				    	}
				    	// Stergem '/' de la finalul stringului
				    	// cmd_parts[1][cmd_parts[1].length() -1];
				    	int index_port = cmd_parts[1].find(":");
				    	int index_path = cmd_parts[1].find("/");
				    	int server_port = 80; // Portul default
				    	cout << index_port << " " << index_path << endl;

				    	if (index_port != -1) { // Extragem portul din URL daca acesta exista
				    		server_port = atoi(cmd_parts[1].substr(index_port+1, index_path -1).c_str());
				    	}
				    	char path[cmd_parts[1].length() - index_path];
				    	strcpy(path, cmd_parts[1].substr(index_path, cmd_parts[1].length()).c_str());
				    	cout << path;
				    	if (index_port != -1) {
				    		cmd_parts[1][index_port] = 0;
				    	} else {
				    		cmd_parts[1][index_path] = 0;
				    	}
				    	char host[cmd_parts[1].length()];
				    	strcpy(host, cmd_parts[1].c_str());cout << endl << host << " " << endl;

				    	hostent *record = gethostbyname(host);
				    	in_addr * address = (in_addr * )record->h_addr;
						string ip_address = inet_ntoa(* address);
				    	const char *ip = ip_address.c_str();

				    	/* formarea adresei serverului */
					    memset(&serv_addr, 0, sizeof(serv_addr));
					    serv_addr.sin_family = AF_INET;
					    serv_addr.sin_port = htons(server_port);

					    cout << ip << endl;

					    if (inet_aton(ip, & serv_addr.sin_addr) <= 0) {
					        printf("Adresa IP invalida.\n");
					        exit(-1);
					    }

					    /*  conectare la server  */
					    if ((serversockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
					        printf("Eroare la  creare socket.\n");
					        exit(-1);
					    }
					    if (connect(serversockfd, (struct sockaddr * ) & serv_addr, sizeof(serv_addr)) < 0) {
					        printf("Eroare la conectare\n");
					        exit(-1);
					    }

					    sprintf(sendbuf, "%s %s %s\nHost: %s\n",
					    			method, path, http, host);
					    send_command(serversockfd, sendbuf, (char*) "200");
					    recv_msg(serversockfd, i, (char*) "200");
					}
				} 
			}
		}
    }
    close(sockfd);

    return 0; 
}
