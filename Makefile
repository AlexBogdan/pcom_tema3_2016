build:
	g++ httpproxy.cpp -o httpproxy
run:
	./httpproxy 1925
check:
	python proxy_tester.py httpproxy
clean:
	rm httpproxy
